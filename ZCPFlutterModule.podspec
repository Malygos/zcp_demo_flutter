Pod::Spec.new do |s|
  s.name             = 'ZCPFlutterModule'
  s.version          = '1.0.0'
  s.summary          = 'ZCP Flutter Module.'
  s.description      = <<-DESC
This is zcp Flutter Module.
                       DESC
  s.homepage         = 'https://gitlab.com/Malygos/zcp_demo_flutter'
  s.license          = { :type => 'MIT' }
  s.author           = { 'zhuchaopeng' => '164757979@qq.com' }
  s.source           = { :git => 'https://gitlab.com/Malygos/zcp_demo_flutter.git', :tag => s.version.to_s }
  s.ios.deployment_target = '9.0'
  s.default_subspec  = 'Default'
  
  s.subspec "Default" do |ss|
    ss.dependency 'ZCPFlutterModule/Flutter'
    ss.dependency 'ZCPFlutterModule/FlutterPluginRegistrant'
    ss.dependency 'ZCPFlutterModule/App'
  end

  s.subspec "Flutter" do |ss|
    ss.vendored_frameworks = 'ZCPFlutterModule/Release/Flutter.framework'
  end

  s.subspec "FlutterPluginRegistrant" do |ss|
    ss.source_files = 'ZCPFlutterModule/Release/FlutterPluginRegistrant/*.{h,m}'
  end

  s.subspec "App" do |ss|
    ss.vendored_frameworks = 'ZCPFlutterModule/Release/App.framework'
    ss.dependency 'ZCPFlutterModule/Flutter'
  end

end
