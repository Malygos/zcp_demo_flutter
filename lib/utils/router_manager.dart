import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/utils/native_interaction_utils.dart';

abstract class ZCPRouter {
  Widget? getPage(String url, dynamic params);
  Map<String, WidgetBuilder>? routes();
}

class ZCPRouterManager {
  static List routers = [];
  static Map<String, WidgetBuilder> allRoutes = {};

  ZCPRouterManager.registerRouter(ZCPRouter router) {
    routers.add(router);
    var routes = router.routes();
    print('zcplog: $routes');
    allRoutes.addAll(routes ?? {});
  }

  ZCPRouterManager.pushNoParams(BuildContext context, String url) {
    ZCPRouterManager.push(context, url, null);
  }

  ZCPRouterManager.push(BuildContext context, String url, dynamic params) {
    for (var router in routers) {
      var page = router.getPage(url, null);
      if (page != null) {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return page;
        }));
        break;
      }
    }
  }

  ZCPRouterManager.pop(BuildContext context, dynamic params) {
    var canPop = Navigator.of(context).canPop();

    if (canPop) {
      Navigator.pop(context);
    } else {
      final Future result = NativeInterfactionUtils.callMethod('backToNative', {'a': 'a', 'b': 'b'});
      result.then((value) {
        print('value is: $value');
      }).catchError((error){
        print('error is $error');
      });
    }
  }
}