import 'package:flutter/services.dart';

class NativeInterfactionUtils {
  static const String NATIVE_CHANNEL_NAME = 'com.zcp.flutter.method.channel';
  static const _channel = const MethodChannel(NATIVE_CHANNEL_NAME);

  static Future<Map> callMethod(methodName, [dynamic arguments]) async {
    try {
      print('--------- $methodName $arguments');
      Map result = await _channel.invokeMethod(methodName, arguments);
      return result;
    } on PlatformException catch (e) {
      return {};
    }
  }

  static void log(String log) {
    NativeInterfactionUtils.callMethod('log', log);
  }

  // static registerHandler() {
  //   _channel.setMethodCallHandler((call) {
  //     switch (call.method) {
  //       case 'backToNative':
  //         break;
  //     }
  //     return ;
  //   });
  // }
}