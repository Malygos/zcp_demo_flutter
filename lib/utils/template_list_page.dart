import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/utils/router_manager.dart';

class TemplateListPage extends StatelessWidget {
  final String title;
  final List<Map>? pageInfos;

  TemplateListPage(
    this.title, {
    Key? key,
    this.pageInfos,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => ZCPRouterManager.pop(context, null),
        ),
      ),
      body: SafeArea(
        child: ListView(
          children: _getItems(context),
        ),
      ),
    );
  }

  List<Widget> _getItems(BuildContext context) {
    if (pageInfos == null) {
      return [];
    }

    List<TemplateListItem> items = [];
    for (var map in pageInfos!) {
      var title = map['title'];
      if (map.containsKey('url')) {
        var url = map['url'];
        var item = TemplateListItem(title, () => ZCPRouterManager.pushNoParams(context, url));
        items.add(item);
      } else if (map.containsKey('list')) {
        var list = map['list'];
        var item = TemplateListItem(title, () => {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return TemplateListPage(title, pageInfos: list,);
          })),
        });
        items.add(item);
      }
    }
    return items;
  }
}

class TemplateListItem extends StatelessWidget {
  final String text;
  final VoidCallback? onPressed;

  TemplateListItem(this.text, this.onPressed, {Key? key}): super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: GestureDetector(
        onTap: onPressed,
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.only(left: 15),
          alignment: Alignment.centerLeft,
          child: Text(text, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold,),),
        ),
      ),
    );
  }
}
