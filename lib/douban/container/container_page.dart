import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/douban/group/page/group_page.dart';
import 'package:zcp_demo_flutter/douban/home/page/home_page.dart';
import 'package:zcp_demo_flutter/douban/market/page/market_page.dart';
import 'package:zcp_demo_flutter/douban/my/page/my_page.dart';
import 'package:zcp_demo_flutter/douban/video/page/video_page.dart';

class ContainerPage extends StatefulWidget {
  ContainerPage({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _ContainerPageState();
}

class _ContainerPageState extends State<ContainerPage> {
  final defaultItemColor = Color.fromARGB(255, 125, 125, 125);
  final itemInfos = [
    _TabBarItemInfo('首页', '', ''),
    _TabBarItemInfo('书影音', '', ''),
    _TabBarItemInfo('小组', '', ''),
    _TabBarItemInfo('市场', '', ''),
    _TabBarItemInfo('我的', '', ''),
  ];

  late List<Widget> pages;
  late List<BottomNavigationBarItem> tabBarItems;
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    pages = [
      HomePage(),
      VideoPage(),
      GroupPage(),
      MarketPage(),
      MyPage()
    ];
    tabBarItems = _createTabBarItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _getPageWidget(0),
          _getPageWidget(1),
          _getPageWidget(2),
          _getPageWidget(3),
          _getPageWidget(4),
        ],
      ),
      backgroundColor: Color.fromARGB(255, 248, 248, 248),
      bottomNavigationBar: BottomNavigationBar(
        items: tabBarItems,
        iconSize: 24,
        currentIndex: 0,
        fixedColor: Color.fromARGB(255, 0, 188, 96),
        type: BottomNavigationBarType.fixed,
        onTap: (int index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }

  List<BottomNavigationBarItem> _createTabBarItems() {
    return itemInfos.map((info) => BottomNavigationBarItem(
      label: info.name,
      icon: Image.asset(info.normalIcon, width: 30, height: 30,),
      activeIcon: Image.asset(info.activeIcon, width: 30, height: 30,)
    )).toList();
  }

  Widget _getPageWidget(int index) {
    return Offstage(
      offstage: _selectedIndex != index,
      child: TickerMode(
        enabled: _selectedIndex == index,
        child: pages[index],
      ),
    );
  }
}

class _TabBarItemInfo {
  String name, normalIcon, activeIcon;
  _TabBarItemInfo(this.name, this.normalIcon, this.activeIcon);
}