import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/douban/container/container_page.dart';
import 'package:zcp_demo_flutter/utils/router_manager.dart';

class DoubanRouter implements ZCPRouter {
  static const scheme_douban_container = 'flutter://douban';

  @override
  Widget? getPage(String url, params) {
    if (url.startsWith('https') || url.startsWith('http')) {
      return null;
    } else {
      switch (url) {
        case scheme_douban_container:
          return ContainerPage();
      }
    }
    return null;
  }

  @override
  Map<String, WidgetBuilder>? routes() {
    return {
      scheme_douban_container : (context) => ContainerPage(),
    };
  }
}