import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zcp_demo_flutter/utils/template_list_page.dart';
import 'package:zcp_demo_flutter/utils/router_manager.dart';
import 'package:zcp_demo_flutter/douban/douban_router.dart';
import 'package:zcp_demo_flutter/learning/learning_router.dart';

void main() {
  ZCPRouterManager.registerRouter(DoubanRouter());
  ZCPRouterManager.registerRouter(LearningRouter());

  runApp(MyApp());
  
  if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
}

class MyApp extends StatelessWidget {
  final List<Map> pageInfo = [
    {'title':'豆瓣','url': DoubanRouter.scheme_douban_container},
    {'title':'learning','list':[
      {'title':'Common Widget','list':[
        {'title':'Text','url':LearningRouter.scheme_learning_text},
        {'title':'Button','url':LearningRouter.scheme_learning_button},
        {'title':'Image','list':[
          {'title':'Image','url':LearningRouter.scheme_learning_image},
          {'title':'ImageFit','url':LearningRouter.scheme_learning_image_fit},
        ]},
        {'title':'TextField','url':LearningRouter.scheme_learning_text_field},
        {'title':'Form','url':LearningRouter.scheme_learning_form},
        {'title':'Scroll','list':[
          {'title':'ListView','url':LearningRouter.scheme_learning_list},
          {'title':'GridView','url':LearningRouter.scheme_learning_grid},
          {'title':'Slivers','url':LearningRouter.scheme_learning_slivers},
          {'title':'ScrollController','url':LearningRouter.scheme_learning_scroll_controller},
        ]},
      ]},
      {'title':'Layout','list':[
        {'title':'Layout','url':LearningRouter.scheme_learning_layout},
        {'title':'Flex-Row','url':LearningRouter.scheme_learning_layout_row},
        {'title':'Flex-Column','url':LearningRouter.scheme_learning_layout_column},
        {'title':'Flex-Expanded','url':LearningRouter.scheme_learning_layout_expanded},
        {'title':'Flex-Stack','url':LearningRouter.scheme_learning_layout_stack},
      ]},
      {'title':'Network','url':LearningRouter.scheme_learning_network},
      {'title':'Animation','url':LearningRouter.scheme_learning_animation},
      {'title':'Event','url':LearningRouter.scheme_learning_event},
      {'title':'State','url':LearningRouter.scheme_learning_state},
      {'title':'Screen','url':LearningRouter.scheme_learning_screen},
    ]},
  ];

  @override
  Widget build(BuildContext context) {
    // CupertinoApp
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(backgroundColor: Colors.white),
      home: TemplateListPage('ZCPDemo', pageInfos: pageInfo,),
      routes: ZCPRouterManager.allRoutes,
    );
  }
}

/*
MaterialApp，表示整个应用将会采用MaterialApp的风格
Scaffold，脚手架，搭建页面基本结构
Widget
  StatelessWidget，没有状态改变的Widget，数据必须被定义为final，
  StatefulWidget，可能出现状态改变的Widget
StatelessWidget
  build，必须重写该方法，方法返回需要渲染的Widget内容
  build执行时机：
    1.第一次被插入到Widget树中
    2.父Widget发生改变时
    3.若依赖的InheritedWidget数据发生改变时
StatefulWidget
  Flutter将StatefulWidget设计成了两个类：
    StatefulWidget，一个类需要继承StatefulWidget作为Widget树的一部分
      createState，必须重写该方法，方法返回创建的状态组件
    State，一个继承State记录状态
      build，必须重写该方法，方法返回需要渲染的Widget内容
      setState，当状态改变时需要调用该方法来重新构建Widget
  生命周期
    StatefulWidget：Constructor - createState
    State：Constructor - mounted - initState - dirty state - build - clean state - dispose
      clean state - setState/didUpdateWidget（手动更新状态/父Widget更新状态） - build
*/