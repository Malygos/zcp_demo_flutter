import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/douban/container/container_page.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';
import 'package:zcp_demo_flutter/utils/template_list_page.dart';

class LearningLayoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Layout', items: [
      LearningTemplateListRowItem(title: 'Align', children: [
        LearningTemplateContainer(width: 40, height: 40,
          child: Align(alignment: Alignment.topLeft, child: Container(width: 20, height: 20, color: Colors.red,),),
        ),
        LearningTemplateContainer(width: 40, height: 40,
          child: Align(alignment: Alignment.topCenter, child: Container(width: 20, height: 20, color: Colors.red,),),
        ),
        LearningTemplateContainer(width: 40, height: 40,
          child: Align(alignment: Alignment.topRight, child: Container(width: 20, height: 20, color: Colors.red,),),
        ),
        LearningTemplateContainer(width: 40, height: 40,
          child: Align(alignment: Alignment.center, child: Container(width: 20, height: 20, color: Colors.red,),),
        ),
        LearningTemplateContainer(width: 40, height: 40,
          child: Align(alignment: Alignment.bottomLeft, child: Container(width: 20, height: 20, color: Colors.red,),),
        ),
        LearningTemplateContainer(width: 40, height: 40,
          child: Align(alignment: Alignment.bottomCenter, child: Container(width: 20, height: 20, color: Colors.red,),),
        ),
        LearningTemplateContainer(width: 40, height: 40,
          child: Align(alignment: Alignment.bottomRight, child: Container(width: 20, height: 20, color: Colors.red,),),
        ),
      ]),
      LearningTemplateListRowItem(title: 'Center', children: [
        LearningTemplateContainer(width: 40, height: 40,
          child: Center(child: Container(width: 20, height: 20, color: Colors.red,),),
        ),
      ]),
      LearningTemplateListRowItem(title: 'Padding', children: [
        LearningTemplateContainer(width: 40, height: 40,
          child: Padding(padding: EdgeInsets.fromLTRB(10, 5, 10, 5), child: Container(color: Colors.red,),),
        ),
        LearningTemplateContainer(width: 40, height: 40,
          child: Padding(padding: EdgeInsets.fromLTRB(5, 10, 5, 10), child: Container(color: Colors.red,),),
        ),
      ]),
      LearningTemplateListRowItem(title: 'Container', children: [
        Container(width: 40, height: 40,
          color: Colors.red,
        ),
        Container(width: 40, height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(color: Colors.red, width: 2, style: BorderStyle.solid),
          ),
        ),
        Container(width: 40, height: 40,
          child: Container(width: 40, height: 40,
            margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
            color: Colors.red,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(color: Colors.red, width: 2, style: BorderStyle.solid),
          ),
        ),
        Container(width: 40, height: 40,
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Container(color: Colors.red,),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(color: Colors.red, width: 2, style: BorderStyle.solid),
          ),
        ),
        Container(width: 40, height: 40,
          child: Icon(Icons.pets),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(color: Colors.red, width: 2, style: BorderStyle.solid),
            boxShadow: [BoxShadow(offset: Offset(5, 5), color: Colors.purple, blurRadius: 5),],
            gradient: LinearGradient(colors: [Colors.greenAccent, Colors.redAccent],),
          ),
        ),
        Container(width: 40, height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(image: AssetImage('assets/images/Hutao.png')),
          ),
        ),
      ]),
    ]);
  }
}


final List<Widget> icons = [
  Icon(Icons.pets, size: 20, color: Colors.red,),
  Icon(Icons.pets, size: 30, color: Colors.green,),
  Icon(Icons.pets, size: 40, color: Colors.blue,),
  Icon(Icons.pets, size: 30, color: Colors.green,),
  Icon(Icons.pets, size: 20, color: Colors.red,),
];
final List<Widget> containers = [
  Container(width: 30, height: 30, color: Colors.red,),
  Container(width: 30, height: 30, color: Colors.green,),
  Container(width: 30, height: 30, color: Colors.blue,),
  Container(width: 30, height: 30, color: Colors.green,),
  Container(width: 30, height: 30, color: Colors.red,),
];

// --------------------------------------------------
// Row
// --------------------------------------------------
class LearningLayoutRowPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Flex-Row', items: [
      LearningTemplateListColumnItem(
        title: 'Row'
            '\nTextDirection，主轴(MainAxis)水平方向'
            '\n    - ltr，水平方向为从左到右，➡️（默认）'
            '\n    - rtl，水平方向为从右到左，⬅️'
            '\nVerticalDirection，交叉轴(CrossAxis)垂直方向'
            '\n    - up，垂直方向为从下到上，⬆️'
            '\n    - down，垂直方向为从上到下，⬇️（默认）',
        children: [
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(
              children: icons,
              mainAxisSize: MainAxisSize.max,
              textDirection: TextDirection.ltr,
              verticalDirection: VerticalDirection.down,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
          ),
        ],
      ),
      LearningTemplateListColumnItem(
        title: 'MainAxisAlignment'
            '\n    - start，左对齐（TextDirection.ltr则右对齐）'
            '\n    - end，右对齐（TextDirection.rtl则左对齐）'
            '\n    - center，居中对齐'
            '\n    - spaceBetween，间隔均分且两端无间隔'
            '\n    - spaceAround，控件均分'
            '\n    - spaceEvenly，间隔均分',
        children: [
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, mainAxisAlignment: MainAxisAlignment.start,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, mainAxisAlignment: MainAxisAlignment.end,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, mainAxisAlignment: MainAxisAlignment.center,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, mainAxisAlignment: MainAxisAlignment.spaceBetween,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, mainAxisAlignment: MainAxisAlignment.spaceAround,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, mainAxisAlignment: MainAxisAlignment.spaceEvenly,),
          ),
        ]),
      LearningTemplateListColumnItem(
        title: 'CrossAxisAlignment'
            '\n    - start，上对齐（VerticalDirection.down下对齐）'
            '\n    - end，下对齐（VerticalDirection.down上对齐）'
            '\n    - center，居中对齐'
            '\n    - stretch，交叉轴方向填满'
            '\n    - spaceAround，控件均分',
        children: [
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, crossAxisAlignment: CrossAxisAlignment.start,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, crossAxisAlignment: CrossAxisAlignment.end,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: icons, crossAxisAlignment: CrossAxisAlignment.center,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: 50,
            child: Row(children: containers, crossAxisAlignment: CrossAxisAlignment.stretch,),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: [Text('我爱',style: TextStyle(fontSize: 30),),Text('中国',style: TextStyle(fontSize: 100),),],
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
            ),
          ),
          SizedBox(height: 10,),
          LearningTemplateContainer(width: double.infinity, height: null,
            child: Row(children: [Text('我爱',style: TextStyle(fontSize: 30),),Text('中国',style: TextStyle(fontSize: 100),),],
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.ideographic,
            ),
          ),
        ],
      ),
      LearningTemplateListRowItem(
        title: '',
        children: [
        ],
      ),
    ],);
  }
}


// --------------------------------------------------
// Column
// --------------------------------------------------
class LearningLayoutColumnPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Flex-Column', items: [
      LearningTemplateListItem(height: 200,
        title: 'Column'
            '\nTextDirection，交叉轴(CrossAxis)水平方向'
            '\n    - ltr，水平方向为从左到右，➡️（默认）'
            '\n    - rtl，水平方向为从右到左，⬅️'
            '\nVerticalDirection，主轴(MainAxis)垂直方向'
            '\n    - up，垂直方向为从下到上，⬆️'
            '\n    - down，垂直方向为从上到下，⬇️（默认）',
        child: Column(
          children: icons,
          mainAxisSize: MainAxisSize.max,
          textDirection: TextDirection.ltr,
          verticalDirection: VerticalDirection.down,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
      ),
    ]);
  }
}


// --------------------------------------------------
// Expanded
// --------------------------------------------------
class LearningLayoutExpandedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Flex-Expanded', items: [
      LearningTemplateListItem(
        title: 'Expanded，绿蓝固定宽高，红:黄=2:1占据剩余宽度',
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: Container(color: Colors.red, height: 60,),
              flex: 2,
            ),
            Container(color: Colors.green, width: 80, height: 80,),
            Container(color: Colors.blue, width: 70, height: 70,),
            Expanded(
              child: Container(color: Colors.yellow, height: 100,),
              flex: 1,
            ),
          ],
        ),
      ),
    ]);
  }
}


// --------------------------------------------------
// Stack
// --------------------------------------------------
class LearningLayoutStackPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Flex-Stack', items: [
      LearningTemplateListItem(
        title: 'Stack',
        child: Stack(
          children: [
            Container(color: Colors.purpleAccent, height: 300,),
            Positioned(left: 20, top: 20,child: Icon(Icons.favorite, size: 50, color: Colors.white,)),
            Positioned(bottom: 20, right: 20,child: Text('你好', style: TextStyle(fontSize: 20, color: Colors.white),)),
          ],
        ),
      ),
    ]);
  }
}