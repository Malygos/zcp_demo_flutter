import 'dart:developer';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';
import 'package:provider/provider.dart';

class LearningStatePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LearningStateState();
}

class _LearningStateState extends State<LearningStatePage> {
  int count1 = 0;
  int count2 = 0;

  @override
  Widget build(BuildContext context) {
    print('LearningStatePage rebuild!!!');
    return LearningTemplatePage(items: [
      LearningTemplateListRowItem(children: [
        IconButton(icon: Icon(Icons.add), onPressed: () {
          setState(() {
            count1++;
          });
        }),
        Text('count: $count1'),
        Text('Random: ${Random().nextInt(100)}'),
      ],),
      _CountInheritedWidget(
        count: count2,
        child: LearningTemplateListRowItem(children: [
          IconButton(icon: Icon(Icons.add), onPressed: () {
            // setState(() {
            //   count2++;
            // });
            count2 += 1;
          }),
          _LearningStateTestWidge1(),
          Builder(builder: (context) {
            return Text('count:${_CountInheritedWidget.of(context).count}(r: ${Random().nextInt(100)})');
          }),
          Text('Random: ${Random().nextInt(100)}'),
        ],),
      ),
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => CounterProvider(),),
        ],
        child: LearningTemplateListRowItem(
          children: [
            Builder(builder: (ctx) {
              return IconButton(icon: Icon(Icons.add), onPressed: () {
                print('按钮重新build'); // 点击按钮之后这里会走
                ctx.read<CounterProvider>().increment();
              });
            }),
            Consumer<CounterProvider>(builder: (ctx, provider, child) {
              print('Text1重新build');
              return Text('count:${provider.count}(r: ${Random().nextInt(100)})');
            },),
            Builder(builder: (ctx) {
              print('Text2重新build');
              return Text('count:${ctx.watch<CounterProvider>().count}(r: ${Random().nextInt(100)})');
            }),
          ],
        ),
      ),
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => CounterProvider(),),
        ],
        child: LearningTemplateListRowItem(
          children: [
            Selector<CounterProvider, CounterProvider>(
              selector: (ctx, provider) => provider,
              shouldRebuild: (pre, next) => false,
              builder: (ctx, pvd, child) {
                print('按钮重新build'); // 点击按钮之后这里不会走
                return IconButton(icon: Icon(Icons.add), onPressed: () {
                  ctx.read<CounterProvider>().increment();
                });
              },
            ),
            Consumer<CounterProvider>(builder: (ctx, provider, child) {
              print('Text1重新build');
              return Text('count:${provider.count}(r: ${Random().nextInt(100)})');
            },),
            Builder(builder: (ctx) {
              print('Text2重新build');
              return Text('count:${ctx.watch<CounterProvider>().count}(r: ${Random().nextInt(100)})');
            }),
          ],
        ),
      ),
    ]);
  }
}


/*
* InheritedWidget
* */
class _CountInheritedWidget extends InheritedWidget {
  final int count;

  _CountInheritedWidget({
    Key? key,
    required Widget child,
    required this.count,
  }) : super(key: key, child: child);

  static _CountInheritedWidget of(BuildContext context) {
    _CountInheritedWidget? result = context.dependOnInheritedWidgetOfExactType<_CountInheritedWidget>();
    return result!;
  }

  @override
  bool updateShouldNotify(_CountInheritedWidget oldWidget) {
    return count != oldWidget.count;
  }
}

class _LearningStateTestWidge1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // 共享数据
    final counter = _CountInheritedWidget.of(context);
    return Text('count: ${counter.count}(r: ${Random().nextInt(100)})');
  }
}


/*
* Provider
* */
class CounterProvider extends ChangeNotifier {
  int _count = 0;
  int get count => _count;

  void increment() {
    _count++;
    notifyListeners(); // 此处通知所有的Customer来重新build
  }
}