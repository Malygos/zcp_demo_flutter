import 'dart:math';
import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';

class LearningAnimationPage extends StatelessWidget {
  final GlobalKey<_LearningAnimation01State> demo01Key = GlobalKey();
  final GlobalKey<_LearningAnimation02State> demo02Key = GlobalKey();
  final GlobalKey<_LearningAnimation03State> demo03Key = GlobalKey();
  final GlobalKey<_LearningAnimation04State> demo04Key = GlobalKey();
  final String imagePath = 'assets/images/Hutao.png';

  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Animation', items: [
      LearningTemplateListItem(height: 150, title: '通过setState实现动画\n由于动画执行期间会频繁重新build Widget，所以性能不好',
        child: GestureDetector(
          onTap: () => _startAnim(demo01Key.currentState?.animController),
          child: LearningAnimation01(key: demo01Key,),
        ),
      ),
      LearningTemplateListItem(height: 150, title: 'AnimatedWidget',
        child: GestureDetector(
          onTap: () => _startAnim(demo02Key.currentState?.animController),
          child: LearningAnimation02(key: demo02Key,),
        ),
      ),
      LearningTemplateListItem(height: 150, title: 'AnimatedBuilder',
        child: GestureDetector(
          onTap: () => _startAnim(demo03Key.currentState?.animController),
          child: LearningAnimation03(key: demo03Key,),
        ),
      ),
      LearningTemplateListItem(height: 150, title: '动画组',
        child: GestureDetector(
          onTap: () => _startAnim(demo04Key.currentState?.animController),
          child: LearningAnimation04(key: demo04Key,),
        ),
      ),
      LearningTemplateListItem(height: 150, title: 'Hero',
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return LearningAnimation05(imagePath: imagePath,);
            }));
          },
          child: Hero(
            tag: imagePath,
            child: Image.asset(imagePath),
          ),
        ),
      ),
    ],);
  }

  void _startAnim(AnimationController? animController) {
    if (animController?.isAnimating ?? false) {
      animController?.stop();
    } else {
      animController?.forward();
    }
  }
}


// --------------------------------------------------
// demo01
// --------------------------------------------------
class LearningAnimation01 extends StatefulWidget {
  LearningAnimation01({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _LearningAnimation01State();
}

class _LearningAnimation01State extends State<LearningAnimation01> with SingleTickerProviderStateMixin {
  late Animation<double> anim;
  late AnimationController animController;

  @override
  void initState() {
    super.initState();

    animController = AnimationController(vsync: this, duration: Duration(seconds: 1));
    anim = CurvedAnimation(parent: animController, curve: Curves.elasticInOut, reverseCurve: Curves.easeOut);
    anim.addListener(() {
      // 这个方法会疯狂调用
      print('触发监听');
      setState(() {});
    });
    anim.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animController.forward();
      }
    });
    anim = Tween<double>(begin: 50, end: 120,).animate(animController);
  }

  @override
  Widget build(BuildContext context) => Icon(Icons.favorite, color: Colors.red, size: anim.value,);

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }
}


// --------------------------------------------------
// demo02，AnimatedWidget
// --------------------------------------------------
class LearningAnimationIcon extends AnimatedWidget {
  LearningAnimationIcon(Animation<double> anim) : super(listenable: anim);
  @override
  Widget build(BuildContext context) {
    final anim = listenable as Animation<double>;
    return Icon(Icons.favorite, color: Colors.red, size: anim.value);
  }
}

class LearningAnimation02 extends StatefulWidget {
  LearningAnimation02({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _LearningAnimation02State();
}

class _LearningAnimation02State extends State<LearningAnimation02> with SingleTickerProviderStateMixin {
  late Animation<double> anim;
  late AnimationController animController;

  @override
  void initState() {
    super.initState();

    animController = AnimationController(vsync: this, duration: Duration(seconds: 1));
    anim = CurvedAnimation(parent: animController, curve: Curves.elasticInOut, reverseCurve: Curves.easeOut);
    anim.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animController.forward();
      }
    });
    anim = Tween<double>(begin: 50, end: 120,).animate(animController);
  }

  @override
  Widget build(BuildContext context) => LearningAnimationIcon(anim);

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }
}


// --------------------------------------------------
// demo03，AnimatedWidget
// --------------------------------------------------
class LearningAnimation03 extends StatefulWidget {
  LearningAnimation03({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _LearningAnimation03State();
}

class _LearningAnimation03State extends State<LearningAnimation03> with SingleTickerProviderStateMixin {
  late Animation<double> anim;
  late AnimationController animController;

  @override
  void initState() {
    super.initState();

    animController = AnimationController(vsync: this, duration: Duration(seconds: 1));
    anim = CurvedAnimation(parent: animController, curve: Curves.elasticInOut, reverseCurve: Curves.easeOut);
    anim.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animController.forward();
      }
    });
    anim = Tween<double>(begin: 50, end: 120,).animate(animController);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
        animation: anim,
        builder: (context, child) => Icon(Icons.favorite, color: Colors.red, size: anim.value,),
      ),
    );
  }

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }
}


// --------------------------------------------------
// demo04
// --------------------------------------------------
class LearningAnimation04 extends StatefulWidget {
  LearningAnimation04({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _LearningAnimation04State();
}

class _LearningAnimation04State extends State<LearningAnimation04> with SingleTickerProviderStateMixin {
  late AnimationController animController;
  late Animation<double> anim;
  late Animation<Color?> colorAnim;
  late Animation<double> sizeAnim;
  late Animation<double> rotationAnim;

  @override
  void initState() {
    super.initState();

    animController = AnimationController(vsync: this, duration: Duration(seconds: 1));
    anim = CurvedAnimation(parent: animController, curve: Curves.elasticInOut, reverseCurve: Curves.easeOut);
    anim.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        animController.forward();
      }
    });

    anim = Tween<double>(begin: 1, end: 0.5).animate(animController);
    colorAnim = ColorTween(begin: Colors.red, end: Colors.orange,).animate(animController);
    sizeAnim = Tween(begin: 50.0, end: 120.0,).animate(animController);
    rotationAnim = Tween(begin: 0.0, end: 2*pi,).animate(animController);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
        animation: animController,
        builder: (context, child) {
          return Opacity(
            opacity: anim.value,
            child: Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationZ(rotationAnim.value),
              child: Container(
                width: sizeAnim.value,
                height: sizeAnim.value,
                color: colorAnim.value,
                alignment: Alignment.center,
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }
}

// --------------------------------------------------
// demo05，Hero
// --------------------------------------------------
class LearningAnimation05 extends StatelessWidget {
  final String imagePath;

  LearningAnimation05({
    Key? key,
    this.imagePath = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Hero(
            tag: imagePath,
            child: Image.asset(
              imagePath,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}