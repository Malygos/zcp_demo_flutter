import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';

class LearningButtonPage extends StatefulWidget {
  @override
  State createState() => LearningButtonPageState();
}

class LearningButtonPageState extends State<LearningButtonPage> {
  int? dropdownButtonValue;

  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Button', items: [
      LearningTemplateListRowItem(title: 'MaterialButton', children: [
        MaterialButton(
          child: Text('Button'),
          onPressed: () => print('点击 MaterialButton'),
        ),
        MaterialButton(
          child: Text('Button'),
          onPressed: () => print('点击 MaterialButton'),
          // 若想全局取消，可以在MaterialApp中的theme里设置这两个属性
          // 取消长按的高亮效果
          highlightColor: Colors.transparent,
          // 取消长按的波纹效果
          splashColor: Colors.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
            side: BorderSide(width: 1, color: Colors.red, style: BorderStyle.solid),
          ),
        ),
      ],),
      LearningTemplateListRowItem(title: 'TextButton', children: [
        TextButton(
          child: Text('TextButton'),
          onPressed: () => print('点击 TextButton'),
        ),
      ],),
      LearningTemplateListRowItem(title: 'IconButton', children: [
        IconButton(
          // icon 的样式，这里使用了 material 内置的 icon 样式
          icon: Icon(Icons.add),
          // icon 的大小
          iconSize: 30,
          // icon 的颜色
          color: Colors.red,
          // 按下时的背景色，此背景色是在 splash 上方的，所以如果不设置透明则看不到 splash
          highlightColor: Color(0x22ff0000),
          // 点击位置会出现一个圆圈扩散的动画，此为设置圆圈颜色
          splashColor: Colors.blue,
          // 点击位置会出现一个圆圈扩散的动画，此为设置圆圈半径
          splashRadius: 50,
          // 长按会在按钮下方显示提示信息，显示时会中断 splash 动画
          // tooltip: '加',
          // 点击事件
          onPressed: () => print('点击 IconButton'),
        ),
        IconButton(
          icon: Icon(Icons.add),
          iconSize: 30,
          hoverColor: Colors.red,
          focusColor: Colors.red,
          onPressed: () => print('点击 IconButton'),
        ),
        // 其实就是一个IconButton，icon为x，带了一个 tooltip='close' 的提示信息
        CloseButton(
          color: Colors.red,
          onPressed: () => print('点击 CloseButton'),
        )
      ],),
      LearningTemplateListRowItem(title: 'DropdownButton', children: [
        DropdownButton<int>(
          hint: Text('请选择'),
          disabledHint: Text('无法选择'),
          // 默认为下箭头
          icon: Icon(Icons.arrow_drop_down),
          iconEnabledColor: Colors.red,
          iconDisabledColor: Colors.grey,
          value: this.dropdownButtonValue,
          onChanged: (value) {
            setState(() => this.dropdownButtonValue = value);
            print('value change to $value');
          },
          items: [
            DropdownMenuItem(value: 0, onTap: () => print('点击 Item0'), child: Text('Item0'),),
            DropdownMenuItem(value: 1, onTap: () => print('点击 Item1'), child: Text('Item1'),),
            DropdownMenuItem(value: 2, onTap: () => print('点击 Item2'), child: Text('Item2'),),
            DropdownMenuItem(value: 3, onTap: () => print('点击 Item3'), child: Text('Item3'),),
          ],
        ),
        DropdownButton<int>(
          hint: Text('请选择'),
          disabledHint: Text('无法选择'),
          icon: Icon(Icons.arrow_drop_down),
          iconEnabledColor: Colors.red,
          iconDisabledColor: Colors.grey,
          // onChanged和items有一个为null，则无法展开
          items: [
            DropdownMenuItem(value: 0, onTap: () => print('点击 Item0'), child: Text('Item0'),),
            DropdownMenuItem(value: 1, onTap: () => print('点击 Item1'), child: Text('Item1'),),
          ],
        ),
        DropdownButton<int>(
          hint: Text('请选择'),
          disabledHint: Text('无法选择'),
          icon: Icon(Icons.arrow_drop_down),
          iconEnabledColor: Colors.red,
          iconDisabledColor: Colors.grey,
          // onChanged和items有一个为null，则无法展开
          onChanged: (value) {},
          items: [],
        ),
        ],
      ),
      LearningTemplateListRowItem(title: 'ElevatedButton', children: [
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            textStyle: const TextStyle(fontSize: 15),
            primary: Colors.white,
            onPrimary: Colors.green,
            onSurface: Colors.blue,
            shadowColor: Colors.transparent,
            side: BorderSide(color: Colors.green, width: 1, style: BorderStyle.solid),
            // 当样式改变时会有隐式动画，这里的设置就是动画的时间
            animationDuration: Duration(milliseconds: 0),
          ),
          onPressed: () => print('点击 ElevatedButton'),
          onLongPress: () => print('长按 ElevatedButton'),
          child: Text('ElevatedButton'),
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 15)),
          onPressed: () => print('点击 ElevatedButton'),
          onLongPress: () => print('长按 ElevatedButton'),
          child: Text('ElevatedButton'),
        ),
      ],),
      LearningTemplateListRowItem(title: 'FloatingActionButton', children: [
        FloatingActionButton(
          onPressed: () => print('点击 FloatingActionButton'),
          child: Icon(Icons.add),
          // 会影响长按时 splash 的显示
          // tooltip: 'add',
          foregroundColor: Colors.white,
          backgroundColor: Colors.blue,
          splashColor: Colors.greenAccent,
          // 阴影偏移量，默认6
          elevation: 0,
          heroTag: null,
        ),
        FloatingActionButton(
          onPressed: () => print('点击 FloatingActionButton'),
          child: Icon(Icons.done),
          heroTag: null,
        )
      ],),
      LearningTemplateListRowItem(title: 'OutlinedButton', children: [
        OutlinedButton(
          child: Text('aaa'),
          onPressed: () => print('点击 OutlinedButton'),
          onLongPress: () => print('长按 OutlinedButton'),
        )
      ],),
      LearningTemplateListRowItem(title: 'PopupMenuButton', children: [
        PopupMenuButton(
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem(child: Text('item0'), value: 0, enabled: false,),
              PopupMenuItem(child: Text('item1'), value: 1, textStyle: TextStyle(color: Colors.white),),
              PopupMenuItem(child: Text('item2'), value: 2,),
              PopupMenuItem(child: Text('item3'), value: 3,),
            ];
          },
          color: Colors.lightBlueAccent,
          onSelected: (int value) => print('点击PopupMenuItem，value=$value'),
          onCanceled: () => print('PopupMenuButton cancel select'),
        ),
        PopupMenuButton(
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem(child: Text('item0'), value: 0, enabled: false,),
              PopupMenuItem(child: Text('item1'), value: 1,),
              PopupMenuItem(child: Text('item2'), value: 2,),
              PopupMenuItem(child: Text('item3'), value: 3,),
            ];
          },
          icon: Icon(Icons.add),
          color: Colors.lightBlueAccent,
          onSelected: (int value) => print('点击PopupMenuItem，value=$value'),
          onCanceled: () => print('PopupMenuButton cancel select'),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: BorderSide(width: 2, color: Colors.redAccent, style: BorderStyle.solid),
          ),
        ),
        PopupMenuButton(
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem(child: Text('item0'), value: 0, enabled: false,),
              PopupMenuItem(child: Text('item1'), value: 1,),
              PopupMenuItem(child: Text('item2'), value: 2,),
              PopupMenuItem(child: Text('item3'), value: 3,),
            ];
          },
          child: Center(child: Text('请选择'),),
          color: Colors.lightBlueAccent,
          onSelected: (int value) => print('点击PopupMenuItem，value=$value'),
          onCanceled: () => print('PopupMenuButton cancel select'),
        ),
      ],),
    ]);
  }
}