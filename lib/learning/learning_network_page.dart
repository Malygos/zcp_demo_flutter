import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:zcp_demo_flutter/learning/model/wxarticle.dart';

class LearningNetworkPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Network', items: [
      LearningTemplateListItem(child:
        MaterialButton(
          child: Text('HttpClient'),
          onPressed: () => _requestByHttpClient(),
        ),
      ),
      LearningTemplateListItem(child:
        MaterialButton(
          child: Text('http'),
          onPressed: () => _requestByHttp(),
        ),
      ),
      LearningTemplateListItem(child:
        MaterialButton(
          child: Text('dio'),
          onPressed: () => _requestByDio(),
        ),
      ),
      LearningTemplateListItem(child:
        MaterialButton(
          child: Text('dio-custom'),
          onPressed: () => _requestByDioCustom(),
        ),
      ),
    ]);
  }

  /*
  * 需要主动关闭request请求，拿到数据也需要手动进行字符串解码
  * */
  void _requestByHttpClient() async {
    final httpClient = HttpClient();
    final url = Uri.parse('https://wanandroid.com/wxarticle/chapters/json');
    final request = await httpClient.getUrl(url);
    final response = await request.close();

    if (response.statusCode == HttpStatus.ok) {
      String data = await response.transform(utf8.decoder).join();
      print(data);
    } else {
      print(response.statusCode);
    }
  }

  /*
  *
  * */
  void _requestByHttp() async {
    final client = http.Client();
    final url = Uri.parse('https://wanandroid.com/wxarticle/chapters/json');
    final response = await client.get(url);
    if (response.statusCode == HttpStatus.ok) {
      print(response.body);
    } else {
      print(response.statusCode);
    }
  }

  void _requestByDio() async {
    final dio = Dio();
    final response = await dio.get('https://wanandroid.com/wxarticle/chapters/json');
    if (response.statusCode == HttpStatus.ok) {
      print(response.data);
    } else {
      print('请求失败：${response.statusCode}');
    }
  }

  void _requestByDioCustom() async {
    LearningHttpRequest.request('https://wanandroid.com/wxarticle/chapters/json', method: 'post', params: {})
        .then((value) {
          final data = value["data"];
          List<WXArticle> models = [];
          for (Map<String, dynamic> map in data) {
            models.add(WXArticle.fromJson(map));
          }
          print(models);
    });
  }
}

class LearningHttpRequest {
  static const baseUrl = 'https://wanandroid.com';
  static const timeout = 5000;
  static final BaseOptions options = BaseOptions(baseUrl: baseUrl, connectTimeout: timeout);
  static final dio = Dio(options);

  static Future<T> request<T>(
        String url, {
        String method = 'get',
        Map<String, dynamic>? params,
        Interceptor? interceptor
        }) async {

    final options = Options(method: method);

    // 创建内部拦截器
    Interceptor inter = InterceptorsWrapper(
      onRequest: (options, handler) {
        print('拦截请求，$options');
        handler.next(options);
      },
      onResponse: (response, handler) {
        print('拦截响应，$response');
        handler.next(response);
      },
      onError: (error, handler) {
        print('拦截错误，$error');
        handler.next(error);
      },
    );

    // 添加拦截器
    List<Interceptor> inters = [inter];
    if (interceptor != null) {
      inters.add(inter);
    }
    dio.interceptors.addAll(inters);

    // 发送请求
    try {
      Response response = await dio.request<T>(url, queryParameters: params, options: options);
      return response.data;
    } on DioError catch(e) {
      return Future.error(e);
    }
  }
}