import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';

class LearningTextPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Text', items: [
      LearningTemplateListItem(title: 'Text', child:
        Text('百善孝为先，论心不论迹，论迹贫家无孝子；万恶淫为首，论迹不论心，论心世上少完人。',
          // 文本样式
          style: TextStyle(
            color: Colors.red,
            fontSize: 18,
            fontWeight: FontWeight.bold,
            backgroundColor: Color(0xffddddddd),
          ),
          // 文字的对齐方向，多行情况下换行处会留一点空间，此时的对齐方向
          textAlign: TextAlign.left,
          //文字的阅读方向
          textDirection: TextDirection.ltr,
          // 超出部分显示方式，ellipsis显示...
          overflow: TextOverflow.ellipsis,
          // 最大行数
          maxLines: 2,
          // 文本缩放
          textScaleFactor: 1.1,
        ),
      ),
      LearningTemplateListRowItem(title: 'TextDecoration', children: [
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.none,),),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline),),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.overline,),),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.lineThrough,),),
      ],),
      LearningTemplateListRowItem(title: 'TextDecorationStyle', children: [
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.solid),),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.double),),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.dotted),),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.dashed),),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.wavy),),
      ],),
      LearningTemplateListRowItem(title: 'decorationColor', children: [
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.solid, decorationColor: Colors.red, decorationThickness: 3,)),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.solid, decorationColor: Colors.green, decorationThickness: 3,)),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.solid, decorationColor: Colors.blue, decorationThickness: 3,)),
        Text('你好', style: TextStyle(fontSize: 20, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.solid, decorationColor: Colors.yellow, decorationThickness: 3,)),
      ]),
      LearningTemplateListItem(title: 'Text.rich', child:
        Text.rich(
          TextSpan(children: [
            TextSpan(text: "《花非花》", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            TextSpan(text: "白居易\n", style: TextStyle(fontSize: 15, decoration: TextDecoration.underline)),
            TextSpan(text: "花非"),
            TextSpan(text: "花", style: TextStyle(color: Colors.red)),
            TextSpan(text: "，雾非"),
            TextSpan(text: "雾", style: TextStyle(color: Colors.red)),
            TextSpan(text: "，\n"),
            TextSpan(text: "夜半来，天明去。\n",),
            TextSpan(text: "来如春梦不多时，\n",),
            TextSpan(text: "去似朝云无觅处。",),
          ],),
          style: TextStyle(fontSize: 20, color: Colors.purple),
          textAlign: TextAlign.center,
        ),
      ),
      LearningTemplateListItem(title: 'RichText', child:
        RichText(
          text: TextSpan(
            children: [
              TextSpan(text: "《送别》", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              TextSpan(text: "李叔同\n", style: TextStyle(fontSize: 15, decoration: TextDecoration.underline)),
              TextSpan(text: "长亭外，古道边，", style: TextStyle(color: Colors.red)),
              TextSpan(text: "芳草碧连天，\n"),
              TextSpan(text: "晚风拂柳笛声残，夕阳山外山。\n"),
              TextSpan(text: "天之涯，海之角，", style: TextStyle(color: Colors.red)),
              TextSpan(text: "知交半零落。\n",),
              TextSpan(text: "一壶浊酒尽余欢，今宵别梦寒。",),
            ],
            style: TextStyle(fontSize: 20, color: Colors.black),
          ),
          textAlign: TextAlign.center,
        ),
      ),
    ],);
  }
}