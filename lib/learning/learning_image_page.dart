import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';

class LearningImagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Image', items: [
      LearningTemplateListRowItem(title: 'Image.asset and Image.network', children: [
        LearningTemplateContainer(child: Image.asset('assets/images/panda.png',),),
        LearningTemplateContainer(child: Image.network('https://www.baidu.com/img/flexible/logo/pc/result@2.png', fit: BoxFit.contain,)),
      ],),
      LearningTemplateListRowItem(title: '圆角图片(CircleAvatar，CircleAvatar，ClipOval，ClipRect)', children: [
        // CircleAvatar，设置背景图会按宽高自动裁剪成圆形，还可以通过child添加其他组件
        LearningTemplateContainer(
          child: CircleAvatar(
            backgroundImage: NetworkImage('https://www.baidu.com/img/flexible/logo/pc/result@2.png'),
            child: Container(
              alignment: Alignment(0, -0.5),
              width: 100,
              height: 100,
              child: Text('百度', style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),),
            ),
          ),
        ),
        LearningTemplateContainer(
          child: CircleAvatar(
            radius: 50,
            backgroundImage: AssetImage('assets/images/Lumine.png'),
            child: Container(
              alignment: Alignment(0, 0.5),
              width: 100, height: 100,
              child: Text('爷真可爱', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            ),
          ),
        ),
        // ClipOval，会自动将child添加的组件按宽高裁剪成圆形
        LearningTemplateContainer(
          child: ClipOval(
            child: Image.asset('assets/images/Hutao.png', width: 100, height: 100,),
          ),
        ),
        // ClipRRect，可指定圆角大小
        LearningTemplateContainer(
          child: ClipRRect(
            child: Image.asset('assets/images/Hutao.png', width: 100, height: 100,),
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ],),
      LearningTemplateListRowItem(title: 'colorBlendMode', children: [
        LearningTemplateContainer(child:
          Image.network(
            'https://www.baidu.com/img/flexible/logo/pc/result@2.png',
            fit: BoxFit.fitWidth,
            color: Colors.greenAccent,
            colorBlendMode: BlendMode.color,
          ),
        ),
        LearningTemplateContainer(child:
          Image.network(
            'https://www.baidu.com/img/flexible/logo/pc/result@2.png',
            fit: BoxFit.fitWidth,
            color: Colors.greenAccent,
            colorBlendMode: BlendMode.colorBurn,
          ),
        ),
        LearningTemplateContainer(child:
          Image.network(
            'https://www.baidu.com/img/flexible/logo/pc/result@2.png',
            fit: BoxFit.fitWidth,
            color: Colors.greenAccent,
            colorBlendMode: BlendMode.colorDodge,
          ),
        ),
      ],),
    ]);
  }
}

class LearningImageFitPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'ImageFit', items: [
      LearningTemplateListRowItem(title: 'fit: BoxFit.fill', children: [
        LearningTemplateContainer(width: 100, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fill,),),
        LearningTemplateContainer(width: 50, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fill,),),
        LearningTemplateContainer(width: 100, height: 50, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fill,),),
      ]),
      LearningTemplateListRowItem(title: 'fit: BoxFit.contain', children: [
        LearningTemplateContainer(width: 100, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.contain,),),
        LearningTemplateContainer(width: 50, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.contain,),),
        LearningTemplateContainer(width: 100, height: 50, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.contain,),),
      ]),
      LearningTemplateListRowItem(title: 'fit: BoxFit.cover', children: [
        LearningTemplateContainer(width: 100, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.cover,),),
        LearningTemplateContainer(width: 50, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.cover,),),
        LearningTemplateContainer(width: 100, height: 50, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.cover,),),
      ]),
      LearningTemplateListRowItem(title: 'fit: BoxFit.fitWidth', children: [
        LearningTemplateContainer(width: 100, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fitWidth,),),
        LearningTemplateContainer(width: 50, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fitWidth,),),
        LearningTemplateContainer(width: 100, height: 50, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fitWidth,),),
      ]),
      LearningTemplateListRowItem(title: 'fit: BoxFit.fitHeight', children: [
        LearningTemplateContainer(width: 100, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fitHeight,),),
        LearningTemplateContainer(width: 50, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fitHeight,),),
        LearningTemplateContainer(width: 100, height: 50, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.fitHeight,),),
      ]),
      LearningTemplateListRowItem(title: 'fit: BoxFit.none', children: [
        LearningTemplateContainer(width: 100, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.none,),),
        LearningTemplateContainer(width: 50, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.none,),),
        LearningTemplateContainer(width: 100, height: 50, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.none,),),
      ]),
      LearningTemplateListRowItem(title: 'fit: BoxFit.scaleDown，与contain不同的是，当容器尺寸大于图片尺寸时图片不会按规则填充容器', children: [
        LearningTemplateContainer(width: 100, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.scaleDown,),),
        LearningTemplateContainer(width: 50, height: 100, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.scaleDown,),),
        LearningTemplateContainer(width: 100, height: 50, child: Image.asset('assets/images/Lumine.png', fit: BoxFit.scaleDown,),),
      ]),
    ]);
  }
}