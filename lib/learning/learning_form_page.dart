import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/douban/container/container_page.dart';
import 'package:zcp_demo_flutter/utils/router_manager.dart';

class LearningFormPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LearningFormState();
}

class _LearningFormState extends State<LearningFormPage> {
  final registerFormKey = GlobalKey<FormState>();
  String? username;
  String? password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Form'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => ZCPRouterManager.pop(context, null),
        ),
      ),
      body: SafeArea(
        child: Form(
          key: registerFormKey,
          child: Container(
            margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.people),
                    labelText: '用户名或手机号',
                    hintText: '请输入姓名',
                  ),
                  onSaved: (value) {
                    this.username = value;
                    print('username onSaved! $value');
                  },
                  validator: (value) {
                    if ((value ?? '').isEmpty) {
                      return '账号不能为空';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    icon: Icon(Icons.lock),
                    labelText: '密码'
                  ),
                  onSaved: (value) {
                    this.password = value;
                    print('password onSaved! $value');
                  },
                  validator: (value) {
                    if ((value ?? '').isEmpty) {
                      return '密码不能为空';
                    }
                    return null;
                  },
                  // autovalidateMode: AutovalidateMode.disabled,
                ),
                SizedBox(height: 16,),
                Container(
                  width: double.infinity,
                  height: 44,
                  child: MaterialButton(
                    child: Text('注册', style: TextStyle(fontSize: 20, color: Colors.white),),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                    color: Colors.lightGreen,
                    onPressed: () {
                      registerFormKey.currentState?.save();
                      registerFormKey.currentState?.validate();
                      print('username: $username, password: $password');
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}