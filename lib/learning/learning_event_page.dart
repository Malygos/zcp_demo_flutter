import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';
import 'package:event_bus/event_bus.dart';

class LearningEventPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LearningEventPage();
}

class _LearningEventPage extends State<LearningEventPage> {
  final eventBus = EventBus();
  String msg = '';

  @override
  void initState() {
    super.initState();
    eventBus.on<String>().listen((event) {
      setState(() {
        msg = 'Random: $event';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'Event', physics: NeverScrollableScrollPhysics(), items: [
      LearningTemplateListItem(title: 'Listener',
        child: Listener(
          child: Container(height: 100, color: Colors.red,),
          onPointerDown: (event) => print('手指按下，$event'),
          onPointerMove: (event) => print('手指移动，$event'),
          onPointerUp: (event) => print('手指抬起，$event'),
          onPointerCancel: (event) => print('onPointerCancel，$event'),
          onPointerHover: (event) => print('onPointerHover，$event'),
          onPointerSignal: (event) => print('onPointerSignal，$event'),
        ),
      ),
      LearningTemplateListItem(title: 'GestureDetector',
        child: GestureDetector(
          child: Container(height: 100, color: Colors.red,),
          onTap: () => print('单击'),
          onTapDown: (details) => print('手指按下，global: ${details.globalPosition}，local: ${details.localPosition}'),
          onTapUp: (details) => print('手指抬起，global: ${details.globalPosition}，local: ${details.localPosition}'),
          onDoubleTap: () => print('双击'),
          onLongPress: () => print('长按'),
          onPanStart: (details) => print('开始拖动'),
          onPanUpdate: (details) => print('拖动移动位置'),
          onPanEnd: (details) => print('结束拖动'),
          onForcePressStart: (details) => print('重按'),
        ),
      ),
      LearningTemplateListRowItem(title: 'event_bus',
        children: [
          GestureDetector(
            child: Container(width: 100, height: 100, color: Colors.red,),
            onTap: () {
              eventBus.fire('${Random().nextInt(100)}');
            },
          ),
          Text(this.msg),
        ],
      ),
    ]);
  }
}