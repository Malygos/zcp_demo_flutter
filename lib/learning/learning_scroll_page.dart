import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';
import 'package:zcp_demo_flutter/utils/router_manager.dart';

final List<Widget> verItems = [
  Container(color: Colors.red, height: 50,),
  Container(color: Colors.green, height: 50,),
  Container(color: Colors.blue, height: 50,),
  Container(color: Colors.yellow, height: 50,),
];

final List<Widget> horItems = [
  Container(color: Colors.red, width: 100,),
  Container(color: Colors.green, width: 100,),
  Container(color: Colors.blue, width: 100,),
  Container(color: Colors.yellow, width: 100,),
];

final List<ListTile> tiles = [
  ListTile(leading: Icon(Icons.people, size: 36), title: Text('昵称'), subtitle: Text('Zcp'), trailing: Icon(Icons.arrow_forward_ios),),
  ListTile(leading: Icon(Icons.phone, size: 36), title: Text('手机号'), subtitle: Text('13800000000'), trailing: Icon(Icons.arrow_forward_ios),),
  ListTile(leading: Icon(Icons.map, size: 36), title: Text('地址'), subtitle: Text('地球村'), trailing: Icon(Icons.arrow_forward_ios),),
];

final List<Icon> icons = [
  Icon(Icons.pets, size: 30,),Icon(Icons.pets, size: 30,),Icon(Icons.pets, size: 30,),
  Icon(Icons.pets, size: 30,),Icon(Icons.pets, size: 30,),Icon(Icons.pets, size: 30,),
  Icon(Icons.pets, size: 30,),Icon(Icons.pets, size: 30,),Icon(Icons.pets, size: 30,),
  Icon(Icons.pets, size: 30,),Icon(Icons.pets, size: 30,),Icon(Icons.pets, size: 30,),
];

final List<Widget> boxes = [
  Container(color: Colors.red, child: Icon(Icons.pets, size: 30, color: Colors.white,),),
  Container(color: Colors.red, child: Icon(Icons.pets, size: 30, color: Colors.white,),),
  Container(color: Colors.red, child: Icon(Icons.pets, size: 30, color: Colors.white,),),
  Container(color: Colors.red, child: Icon(Icons.pets, size: 30, color: Colors.white,),),
  Container(color: Colors.red, child: Icon(Icons.pets, size: 30, color: Colors.white,),),
  Container(color: Colors.red, child: Icon(Icons.pets, size: 30, color: Colors.white,),),
];

// --------------------------------------------------
// ListView
// --------------------------------------------------
class LearningListViewPage extends StatelessWidget {
  final Divider redDivider = Divider(color: Colors.red,);
  final Divider blueDivider = Divider(color: Colors.blue,);

  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'List', items: [
      LearningTemplateListItem(title: '垂直滚动', child: Container(height: 100,
        child: ListView(children: verItems,),),
      ),
      LearningTemplateListItem(title: '水平滚动', child: Container(height: 100,
        child: ListView(scrollDirection: Axis.horizontal, children: horItems,),),
      ),
      LearningTemplateListItem(title: 'ListTile', height: 200, child: Container(height: 100,
        child: ListView(children: tiles,),),
      ),
      LearningTemplateListItem(title: 'ListView.builder', height: 200, child: Container(height: 100,
        // itemCount=null为无限列表
        child: ListView.builder(itemCount: null, itemExtent: 60,
          itemBuilder: (context, index) {
            return ListTile(title: Text('标题$index'), subtitle: Text('详细内容$index'),);
          },
        ),),
      ),
      LearningTemplateListItem(title: 'ListView.separated', height: 200, child: Container(height: 100,
        child: ListView.separated(itemCount: 20,
          itemBuilder: (context, index) {
            return ListTile(title: Text('标题$index'), subtitle: Text('详细内容$index'),);
          },
          separatorBuilder: (context, index) {
            return index % 2 == 0 ? redDivider : blueDivider;
          },
        ),),
      ),
    ],);
  }
}


// --------------------------------------------------
// GridView
// --------------------------------------------------
class LearningGridViewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(items: [
      LearningTemplateListItem(height: 150,
        title: 'SliverGridDelegateWithFixedCrossAxisCount'
            '\n也可用GridView.count实现'
            '\nitem宽度不固定，item均分总宽减去crossAxisSpacing的剩余宽度',
        child: GridView(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3, // 交叉轴的item个数，item均分减掉crossAxisSpacing的宽度
            crossAxisSpacing: 50, // 交叉轴item间距
            mainAxisSpacing: 50, // 主轴item间距
            childAspectRatio: 2, // item的宽高比
          ),
          children: boxes,
        ),
      ),
      LearningTemplateListItem(height: 150,
        title: 'SliverGridDelegateWithMaxCrossAxisExtent'
            '\n也可用GridView.extent实现'
            '\nitem宽度不固定，item均分总宽减去crossAxisSpacing的剩余宽度，同时一行尽量多的放item且宽度不超过maxCrossAxisExtent',
        child: GridView(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 100,//交叉轴item的最大宽度，
            crossAxisSpacing: 20,//交叉轴item间距
            mainAxisSpacing: 40,
            childAspectRatio: 2,
          ),
          children: boxes,
        ),
      ),
      LearningTemplateListItem(height: 150,
        title: 'GridView.builder',
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            crossAxisSpacing: 20,
            mainAxisSpacing: 40,
          ),
          itemBuilder: (context, index) {
            return Container(color: Colors.red, child: Center(child: Text('Item$index'),),);
          },
        ),
      ),
    ]);
  }
}

// --------------------------------------------------
// Slivers
// --------------------------------------------------
class LearningSliversPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 200,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('Slivers'),
              background: Image(image: AssetImage('assets/images/Hutao.png'), fit: BoxFit.cover,),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              crossAxisSpacing: 8,
              mainAxisSpacing: 8,
              childAspectRatio: 4,
            ),
            delegate: SliverChildBuilderDelegate(
                  (context, index){
                return Container(
                  alignment: Alignment.center,
                  color: Colors.teal[100 * (index % 10)],
                  child: Text('item$index', style: TextStyle(fontSize: 15),),
                );
              },
              childCount: 10,
            ),
          ),
          SliverFixedExtentList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.lightBlue[100 * (index % 10)],
                  child: Text('item$index', style: TextStyle(fontSize: 15,),),
                );
              },
              childCount: 10,
            ),
            itemExtent: 50,
          ),
        ],
      ),
    );
  }
}

class LearningScrollControllerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LearningScrollControllerState();
}

class _LearningScrollControllerState extends State<LearningScrollControllerPage> {
  late ScrollController _controller;
  bool _isShowTop = false;
  int _progress = 0;

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(() {
      var tempShowTop = _controller.offset >= 1000;
      if (tempShowTop != _isShowTop) {
        setState(() {
          _isShowTop = tempShowTop;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScrollController'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => ZCPRouterManager.pop(context, null),
        ),
      ),
      body: SafeArea(
        child: NotificationListener(
          child: Stack(
            children: [
              ListView.builder(
                itemCount: 100,
                itemExtent: 60,
                controller: _controller,
                itemBuilder: (context, index) {
                  return ListTile(title: Text('item$index'),);
                },
              ),
              Positioned(top: 10, right: 10,
                child: CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.black54,
                  child: Text('$_progress%'),
                ),
              )
            ],
          ),
          onNotification: (notification) {
            if (notification is ScrollStartNotification) {
              print('开始滚动...');
            } else if (notification is ScrollUpdateNotification) {
              final currentPixel = notification.metrics.pixels;
              final totalPixel = notification.metrics.maxScrollExtent;
              double progress = currentPixel / totalPixel;
              if (progress < 0) {
                progress = 0;
              } else if (progress > 1) {
                progress = 1;
              }
              setState(() {
                _progress = (progress * 100).toInt();
              });
            } else if (notification is ScrollEndNotification) {
              print('结束滚动...');
            }
            return false;
          },
        ),
      ),
      floatingActionButton: !_isShowTop ? null : FloatingActionButton(
        child: Icon(Icons.arrow_upward),
        onPressed: () {
          _controller.animateTo(0, duration: Duration(milliseconds: 1000), curve: Curves.ease);
        },
      ),
    );
  }
}