import 'package:json_annotation/json_annotation.dart';
part 'wxarticle.g.dart'; //`flutter pub run build_runner build`

@JsonSerializable()
class WXArticle {
  int id;
  int courseId;
  String name;
  int order;
  int parentChapterId;
  bool userControlSetTop;
  int visible;

  WXArticle(this.id, this.courseId, this.name, this.order, this.parentChapterId, this.userControlSetTop, this.visible);

  factory WXArticle.fromJson(Map<String, dynamic> json) => _$WXArticleFromJson(json);
  Map<String, dynamic> toJson() => _$WXArticleToJson(this);

  @override
  String toString() {
    return 'WXArticle {id:$id, courseId:$courseId, name:$name, order:$order, parentChapterId:$parentChapterId, userControlSetTop:$userControlSetTop, visible:$visible}';
  }
}