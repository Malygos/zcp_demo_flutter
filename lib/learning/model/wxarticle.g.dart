// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wxarticle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WXArticle _$WXArticleFromJson(Map<String, dynamic> json) {
  return WXArticle(
    json['id'] as int,
    json['courseId'] as int,
    json['name'] as String,
    json['order'] as int,
    json['parentChapterId'] as int,
    json['userControlSetTop'] as bool,
    json['visible'] as int,
  );
}

Map<String, dynamic> _$WXArticleToJson(WXArticle instance) => <String, dynamic>{
      'id': instance.id,
      'courseId': instance.courseId,
      'name': instance.name,
      'order': instance.order,
      'parentChapterId': instance.parentChapterId,
      'userControlSetTop': instance.userControlSetTop,
      'visible': instance.visible,
    };
