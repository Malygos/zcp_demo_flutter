import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';

class LearningTextFieldPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LearningTextFieldState();
}

class _LearningTextFieldState extends State<LearningTextFieldPage> {
  final textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    textEditingController.text = '张三';
    textEditingController.addListener((){
      print('Text Listener: ${textEditingController.text}');
    });
  }

  @override
  Widget build(BuildContext context) {
    return LearningTemplatePage(title: 'TextField', items: [
      LearningTemplateListColumnItem(title: 'TextField', children: [
        LearningTemplateContainer(width: MediaQuery.of(context).size.width - 32,
          child: TextField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              labelText: '姓名',
              hintText: '请输入姓名',
            ),
            onChanged: (value) => print('TextField value changed. $value'),
            onSubmitted: (value) => print('TextField submitted. $value'),
          ),
        ),
        SizedBox(height: 10,),
        LearningTemplateContainer(width: MediaQuery.of(context).size.width - 32,
          child: TextField(
            keyboardType: TextInputType.text,
            style: TextStyle(color: Colors.red),
            controller: textEditingController,
            decoration: InputDecoration(
              icon: Icon(Icons.people),
              labelText: '姓名',
              hintText: '请输入姓名',
              filled: true,
              fillColor: Colors.lightGreen,
            ),
            onChanged: (value) => print('TextField value changed. $value'),
            onSubmitted: (value) => print('TextField submitted. $value'),
          ),
        ),
      ]),
    ]);
  }
}