import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_animation_page.dart';
import 'package:zcp_demo_flutter/learning/learning_button_page.dart';
import 'package:zcp_demo_flutter/learning/learning_event_page.dart';
import 'package:zcp_demo_flutter/learning/learning_screen_page.dart';
import 'package:zcp_demo_flutter/learning/learning_state_page.dart';
import 'package:zcp_demo_flutter/learning/learning_layout_page.dart';
import 'package:zcp_demo_flutter/learning/learning_network_page.dart';
import 'package:zcp_demo_flutter/learning/learning_scroll_page.dart';
import 'package:zcp_demo_flutter/learning/learning_text_field_page.dart';
import 'package:zcp_demo_flutter/utils/router_manager.dart';
import 'package:zcp_demo_flutter/learning/learning_text_page.dart';
import 'package:zcp_demo_flutter/learning/learning_image_page.dart';
import 'package:zcp_demo_flutter/learning/learning_form_page.dart';

class LearningRouter implements ZCPRouter {
  static const scheme_learning_text = 'flutter://learning/text';
  static const scheme_learning_button = 'flutter://learning/button';
  static const scheme_learning_image = 'flutter://learning/image';
  static const scheme_learning_image_fit = 'flutter://learning/imageFit';
  static const scheme_learning_text_field = 'flutter://learning/textField';
  static const scheme_learning_form = 'flutter://learning/form';
  static const scheme_learning_layout = 'flutter://learning/layout';
  static const scheme_learning_layout_row = 'flutter://learning/layoutRow';
  static const scheme_learning_layout_column = 'flutter://learning/layoutColumn';
  static const scheme_learning_layout_expanded = 'flutter://learning/layoutExpanded';
  static const scheme_learning_layout_stack = 'flutter://learning/layoutStack';
  static const scheme_learning_list = 'flutter://learning/list';
  static const scheme_learning_grid = 'flutter://learning/grid';
  static const scheme_learning_slivers = 'flutter://learning/slivers';
  static const scheme_learning_scroll_controller = 'flutter://learning/scrollController';
  static const scheme_learning_network = 'flutter://learning/network';
  static const scheme_learning_animation = 'flutter://learning/animation';
  static const scheme_learning_event = 'flutter://learning/event';
  static const scheme_learning_state = 'flutter://learning/state';
  static const scheme_learning_screen = 'flutter://learning/screen';

  @override
  Widget? getPage(String url, params) {
    if (url.startsWith('https') || url.startsWith('http')) {
      return null;
    } else {
      switch (url) {
        case scheme_learning_text:
          return LearningTextPage();
        case scheme_learning_button:
          return LearningButtonPage();
        case scheme_learning_image:
          return LearningImagePage();
        case scheme_learning_image_fit:
          return LearningImageFitPage();
        case scheme_learning_text_field:
          return LearningTextFieldPage();
        case scheme_learning_form:
          return LearningFormPage();
        case scheme_learning_layout:
          return LearningLayoutPage();
        case scheme_learning_layout_row:
          return LearningLayoutRowPage();
        case scheme_learning_layout_column:
          return LearningLayoutColumnPage();
        case scheme_learning_layout_expanded:
          return LearningLayoutExpandedPage();
        case scheme_learning_layout_stack:
          return LearningLayoutStackPage();
        case scheme_learning_list:
          return LearningListViewPage();
        case scheme_learning_grid:
          return LearningGridViewPage();
        case scheme_learning_slivers:
          return LearningSliversPage();
        case scheme_learning_scroll_controller:
          return LearningScrollControllerPage();
        case scheme_learning_network:
          return LearningNetworkPage();
        case scheme_learning_animation:
          return LearningAnimationPage();
        case scheme_learning_event:
          return LearningEventPage();
        case scheme_learning_state:
          return LearningStatePage();
        case scheme_learning_screen:
          return LearningScreenPage();
      }
    }
    return null;
  }

  @override
  Map<String, WidgetBuilder>? routes() {
    return {
      scheme_learning_text : (context) => LearningTextPage(),
      scheme_learning_button : (context) => LearningButtonPage(),
      scheme_learning_image: (context) => LearningImagePage(),
      scheme_learning_image_fit: (context) => LearningImageFitPage(),
      scheme_learning_text_field: (context) => LearningTextFieldPage(),
      scheme_learning_form: (context) => LearningFormPage(),
      scheme_learning_layout: (context) => LearningLayoutPage(),
      scheme_learning_layout_row: (context) => LearningLayoutRowPage(),
      scheme_learning_layout_column: (context) => LearningLayoutColumnPage(),
      scheme_learning_layout_expanded: (context) => LearningLayoutExpandedPage(),
      scheme_learning_layout_stack: (context) => LearningLayoutStackPage(),
      scheme_learning_list: (context) => LearningListViewPage(),
      scheme_learning_grid: (context) => LearningGridViewPage(),
      scheme_learning_slivers: (context) => LearningSliversPage(),
      scheme_learning_scroll_controller: (context) => LearningScrollControllerPage(),
      scheme_learning_network: (context) => LearningNetworkPage(),
      scheme_learning_animation: (context) => LearningAnimationPage(),
      scheme_learning_event: (context) => LearningEventPage(),
      scheme_learning_state: (context) => LearningStatePage(),
      scheme_learning_screen: (context) => LearningScreenPage(),
    };
  }
}