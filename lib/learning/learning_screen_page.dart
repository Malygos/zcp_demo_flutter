import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:zcp_demo_flutter/learning/learning_template_page.dart';

class LearningScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    print('屏幕尺寸 (width: ${mediaQueryData.size.width}, height: ${mediaQueryData.size.height})');
    print('分辨率 (${window.physicalSize.width} x ${window.physicalSize.height})');
    print('dpr (${window.devicePixelRatio})');
    print('状态栏 (${mediaQueryData.padding.top}) 底部高度 (${mediaQueryData.padding.bottom})');

    return LearningTemplatePage(items: [
      LearningTemplateListItem(
        child: Text('相关尺寸:'
            '\n屏幕尺寸 (width: ${mediaQueryData.size.width}, height: ${mediaQueryData.size.height})'
            '\n分辨率 (${window.physicalSize.width} x ${window.physicalSize.height})'
            '\ndpr (${window.devicePixelRatio})'
            '\n状态栏 (${mediaQueryData.padding.top}) 底部高度 (${mediaQueryData.padding.bottom})',
        ),
      ),
      LearningTemplateListItem(
        child: Text('dpx:'
            '\n不管什么屏幕都统一分成750份'
            '\niPhone 5：1rpx = 320/750 = 0.4266px ≈ 0.42px'
            '\niPhone 6：1rpx = 375/750 = 0.5px'
            '\niPhone 6 plus：1rpx = 414/750 = 0.552px',
        ),
      ),
    ]);
  }
}