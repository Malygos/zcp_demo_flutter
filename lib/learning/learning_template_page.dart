import 'package:flutter/material.dart';

class LearningTemplatePage extends StatelessWidget {
  final String title;
  final List<Widget> items;
  final ScrollPhysics? physics;

  LearningTemplatePage({
    Key? key,
    this.title = '',
    required this.items,
    this.physics,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title),),
      body: SafeArea(
        child: ListView(
          physics: this.physics,
          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
          children: items,
        ),
      ),
    );
  }
}

// 由于Container设置了width: double.infinity，若child使用固定宽高的Container依然会被撑大。
class LearningTemplateListItem extends StatelessWidget {
  final Widget child;
  final String title;
  final double? width;
  final double? height;

  LearningTemplateListItem({
    Key? key,
    this.title = '',
    this.width,
    this.height,
    required this.child
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
          child: Text(title),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          // 占满整个屏幕宽度
          width: this.width ?? double.infinity,
          height: this.height,
          // width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(border: Border.all(color: Colors.blue, style: BorderStyle.solid, width: 1.0,)),
          child: child,
        ),
      ],
    );
  }
}

class LearningTemplateListRowItem extends StatelessWidget {
  final String title;
  final List<Widget> children;
  final MainAxisAlignment mainAxisAlignment;

  LearningTemplateListRowItem({
    Key? key,
    this.title = '',
    required this.children,
    this.mainAxisAlignment = MainAxisAlignment.spaceBetween,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(padding: EdgeInsets.fromLTRB(10, 10, 0, 0), child: Text(title),),
        Container(
          margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          width: double.infinity,
          decoration: BoxDecoration(border: Border.all(color: Colors.blue,style: BorderStyle.solid,width: 1.0,)),
          child: Row(
            mainAxisAlignment: mainAxisAlignment,
            children: children,
          ),
        ),
      ],
    );
  }
}

class LearningTemplateListColumnItem extends StatelessWidget {
  final String title;
  final List<Widget> children;
  final MainAxisAlignment mainAxisAlignment;

  LearningTemplateListColumnItem({
    Key? key,
    this.title = '',
    required this.children,
    this.mainAxisAlignment = MainAxisAlignment.spaceBetween,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(padding: EdgeInsets.fromLTRB(10, 10, 0, 0), child: Text(title),),
        Container(
          margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          width: double.infinity,
          decoration: BoxDecoration(border: Border.all(color: Colors.blue,style: BorderStyle.solid,width: 1.0,)),
          child: Column(
            mainAxisAlignment: mainAxisAlignment,
            children: children,
          ),
        ),
      ],
    );
  }
}

class LearningTemplateContainer extends StatelessWidget {
  final double? width;
  final double? height;
  final Color borderColor;
  final Widget? child;

  LearningTemplateContainer({
    Key? key,
    this.width = 80,
    this.height = 80,
    this.borderColor = Colors.blue,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(border: Border.all(color: borderColor, style: BorderStyle.solid, width: 1.0,)),
      child: child,
    );
  }
}