// 01 hello world
// dart的程序入口为main函数
// 没有返回值
// 有参数，参数为main函数执行的命令行参数
// 语句后需要使用分号结尾
// 字符串可使用单引号或双引号
void main(List<String> args) {
  print('Hello world');
}