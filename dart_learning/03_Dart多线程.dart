// import 'package:flutter/services.dart' show rootBundle;
// import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'dart:isolate';

// 03_Dart多线程

// 耗时操作的处理方式
// 方式1：多线程。开启新线程进行一步操作，再通过线程间通信将数据传递给主线程。如java，c++
// 方式2：单线程+事件循环。将所有事件放入事件队列中，事件被执行可以有阻塞式调用和非阻塞式调用。如js，dart

// Dart 的异步操作主要使用 Future，async，await

// Future 使用过程：
// 1.创建/获取 Future
// 2.通过 .then 的方式来监听 Future 内部执行完成时获取到的结果
// 3.通过 .catchError 的方式来监听 Future 内部执行失败或出现异常的错误信息

// Future 的两种状态
// 1.未完成状态（uncompleted），Future 内部的操作正在执行时的状态。
// 2.完成状态（completed），Future 内部的操作执行完成后的状态。

// Future 的链式调用
// 在 then 中返回值，会在下一个链式的 then 调用回调函数中拿到值

test1() {
  // 将阻塞函数的执行
  print('test1.1 start...');
  var result = getNetworkData();
  print('netwrok data: $result');
  print('test1.1 end...');
  print('----------');

  // 不会阻塞函数的执行
  // 使用 future 的 then 方法和 catchError 来监听
  print('test1.2 start...');
  var future2 = getNetworkData2('张三');
  print(future2);
  future2.then((value) {
    print('[then1] netwrok data: $value');
    return value; // 返回，则下一个 then 可得到 value 值
  }).then((value) {
    print('[then2] netwrok data: $value');
    // 未返回 value，则下一个 then 得不到 value 值
  }).then((value) {
    print('[then3] netwrok data: $value');
  }).catchError((error) {
    print(error);
  });
  print('test1.2 end...');
  print('----------');

  // 不会阻塞函数的执行
  print('test1.3 start...');
  var future3 = getNetworkData2('zcp');
  print(future3);
  future3.then((value) {
    print('netwrok data: $value');
  }).catchError((error) {
    print(error);
  });
  print('test1.3 end...');
  print('----------');
}

String getNetworkData() {
  sleep(Duration(seconds: 1));
  return '{\'name\':\'张三\',\'age\':18}';
}
Future<String> getNetworkData2(String? uid) {
  return Future<String>(() {
    sleep(Duration(seconds: 1));
    if (uid == '张三') {
      return '{\'name\':\'张三\',\'age\':18}';
    } else {
      throw Exception('参数校验不通过');
    }
  });
}


test2() {
  // Future.value
  // 完整的 Future 会直接调用 then 的回调函数
  // 并且then 的回调函数会加入事件队列中，执行是异步的。
  print('test2.1 start...');
  Future.value('test2.1，张三').then((value) {
    print(value);
  });
  print('test2.1 end...');

  // Future.error
  print('test2.2 start...');
  Future.error(Exception('test2.2 错误')).catchError((error) {
    print(error);
  });
  print('test2.2 end...');

  // Future.delayed
  print('test2.3 start...');
  Future.delayed(Duration(seconds: 1), () {
    return 'test2.3';
  }).then((value) {
    print(value);
  });
  print('test2.3 end...');
}

// await/async
// await 会阻断代码执行，会在阻断的地方向外返回一个 Future 对象
// async 修饰的方法的返回值必须是 Future 类型的
test3() {
  // 不使用 await 的情况
  print('test3.1 start...');
  var result1 = getNetworkData31();
  print(result1);
  result1.then((value) {
    print('test3.1 result:' + value);
  });
  print('test3.1 end...');
  print('----------');

  // 使用 await 的情况
  print('test3.2 start...');
  var result2 = getNetworkData32();
  print(result2);
  result2.then((value) {
    print('test3.2 result:' + value);
  });
  print('test3.2 end...');
}

// 不使用 await
Future<String> getNetworkData31() async {
  print('test3.1 get netwrok start...');

  // 这里的 result 是 Future 对象。
  var result = Future.delayed(Duration(seconds: 3), () {
    return '{\'name\':\'张三\',\'age\':18}';
  });

  // 这里不会被阻断，会执行下去
  print('test3.1 get network end1...');
  print('$result, ${result.runtimeType}');
  print('test3.1 get network end2...');
  // 这里需要返回 Future 对象
  return result;
}

// 使用 await
Future<String> getNetworkData32() async {
  print('test3.2 get netwrok start...');

  // 这里使用了 await，将会阻断代码执行等待 result 结果，且此时会向外返回一个Future对象
  // 此处的 result 是 String 类型的值
  var result = await Future.delayed(Duration(seconds: 3), () {
    return '{\'name\':\'张三\',\'age\':18}';
  });

  // 这里会被阻断，若使用 await，则下面这几句代码会等结果返回时才执行
  print('test3.2 get network end1...');
  print('$result, ${result.runtimeType}');
  print('test3.2 get network end2...');
  // 这里返回的是 String 值，将触发前面返回给外部的那个 Future 的 then
  return result;
}


// test4() {
//   getUsers().then((value) {
//     print(value);
//   });
// }

// class User {
//   late String name;
//   late int age;
//   late double height;

//   User({required this.name, required this.age, required this.height});
  
//   User.withMap(Map<String, dynamic> parsedMap) {
//     this.name = parsedMap["name"];
//     this.age = parsedMap["age"];
//     this.height = parsedMap["height"];
//   }
// }

// Future<List<User>> getUsers() async {
//   String jsonStr = await rootBundle.loadString("assets/test04.json");
//   final jsonResult = json.decode(jsonStr);
//   final data = jsonResult["data"];

//   List<User> userList = [];
//   for (var map in data) {
//     userList.add(User.withMap(map));
//   }
//   return userList;
// }


// Dart中有事件循环（event loop）来执行代码，存在一个事件队列（event queue），严格划分还存在一个微任务队列（Microtask queue）
/*
  1.main start
  2.main end
  3.task7（微任务）
  4.task1（event queue 首个任务）
  5.task6（第二个Future没有函数执行体，then 被加入微任务中）
  6.2 3 5？
  7.4（微任务）
  8.8 9 10？
*/
test5() {
  print('[test5] main start');

  Future(() => print('[test5] task1'));
  final future = Future(() => null);
  Future(() => print('[test5] task2')).then((_) {
    print('[test5] task3');
    scheduleMicrotask(() => print('[test5] task4'));
  }).then((_) => print('[test5] task5'));

  future.then((_) => print('[test5] task6'));
  scheduleMicrotask(() => print('[test5] task7'));

  Future(() => print('[test5] task8'))
    .then((_) => Future(() => print('[test5] task9')))
    .then((_) => print('[test5] task10'));

  print('[test5] main end');
}


// isolate通信机制（isolate：隔离）
// isolate类似于线程，每个都有自己的 Event loop 和 queue
test6() async {
  Isolate.spawn(foo, "zcp isolate");

  print('start');
  ReceivePort receivePort = ReceivePort();
  Isolate isolate = await Isolate.spawn<SendPort>(foo2, receivePort.sendPort);
  receivePort.listen((message) {
    print('message: $message');
    receivePort.close();
    isolate.kill(priority: Isolate.immediate);
  });
  print('end');
}

void foo(message) {
  print("新的isolate: $message");
}

void foo2(SendPort port) {
  sleep(Duration(seconds: 2));
  port.send("zcp");
}

main() {
  // print('====================');test1();print('====================\n');
  // print('====================');test2();print('====================\n');
  // print('====================');test3();print('====================\n');
  // print('====================');test4();print('====================\n');
  // print('====================');test5();print('====================\n');
  print('====================');test6();print('====================\n');
}