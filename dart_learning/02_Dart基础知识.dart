// 02_Dart基础知识

// 显式声明
// 值可修改，但不能赋值其他类型
void test1() {
  String name = '张三';
  int age = 50;
  double height = 80.0;
  print('${name}, ${age}, ${height}');

  // error
  // name = 50;
}

// 类型推导
// var/dynamic/const/final
// var，推导类型后仅能赋值同类型的值
// dynamic，可以赋值不同类型的值，不建议使用
// const常量，赋值的内容需在编译器就确定
// final常量，赋值的内容可以动态获取
void test2() {

  // var
  {
    var name = '张三';
    name = '李四';
    print(name.runtimeType);
    // name = 50; // error
  }print('----------');

  // dynamic
  {
    dynamic name = '张三';
    print(name.runtimeType);
    name = 7777;
    print(name.runtimeType);
  }print('----------');

  // final/const
  {
    final name = '张三';
    // name = '李四'; // error
    const age = 18;
    // age = 50; // error

    final NOW_TIME = new DateTime.now().toString();
    // const HOST2 = new DateTime.now().toString(); // error
    const SECRET_KEY = '123456';
    print(NOW_TIME);
  }
}

// 数值类型
// 整数用int，浮点数用double
// 
void test3() {
  {
    // 整数
    int age = 18;
    int hexAge = 0x12;
    print(age);
    print(hexAge);
    // 浮点数
    double height = 12;
    print(height);
  }print('----------');

  // 字符串转换
  {
    var age = int.parse('18');
    var height = double.parse('50.2');
    print('${age} ${age.runtimeType}');
    print('${height} ${height.runtimeType}');

    var ageStr = age.toString();
    var heightStr = height.toString();
    var pi = 3.1415926;
    var piStr = pi.toStringAsFixed(2); // 保留小数点后2位
    print('${ageStr} ${ageStr.runtimeType}');
    print('${heightStr} ${heightStr.runtimeType}');
    print('${piStr} ${piStr.runtimeType}');
  }
}

// 布尔类型
// dart没有非0即真或非空即真
void test4() {
  {
    var flag = true;
    print('${flag} ${flag.runtimeType}');
  }
  {
    // var message = 'hello world';
    // if (message) { // error
    //   print(message);
    // }
  }
}

// 字符串类型
// 变量可通过${expression}的形式来进行字符串拼接，若表达式是一个标识符则{}可省略
void test5() {
  var s1 = 'hello world';
  var s2 = "hello world";
  var s3 = '\'hello world\'';
  var s4 = "\"hello world\", \'zcp\'";

  var longStr = '''11111
  22222
  33333
  44444''';

  print(s1);
  print(s2);
  print(s3);
  print(s4);
  print(longStr);

  var name = '张三';
  var age = 18;
  var height = 50.2;
  print('name: $name, age: $age, height: $height');
}

// 集合类型
// List
// Set
// Map
void test6() {
  // List
  {
    // 明确类型定义
    List<int> numbers = [1, 2, 3];
    print('$numbers, ${numbers.runtimeType}');
    // 隐式类型
    var letters = ['a', 'b', 'c', 'd'];
    print('$letters ${letters.runtimeType}');
  }print('----------');

  // Set
  {
    Set<int> numbers = {1, 1, 3, 4};
    print('$numbers, ${numbers.runtimeType}');
    
    var letters = {'a', 'b', 'c', 'd'};
    print('$letters, ${letters.runtimeType}');
  }print('----------');

  // Map
  {
    Map<String, Object> map1 = {'name': '张三', 'age': 18};
    print('$map1, ${map1.runtimeType}');

    var map2 = {'name': '张三', 'age': 18};
    print('$map2, ${map2.runtimeType}');
  }

  // 集合常见操作
  {
    var list = ['a', 'b', 'c', 'd'];
    var set = {'a', 'b', 'c', 'd'};
    var map = {'name': '张三', 'age': 18};

    // length
    print('${list.length}, ${set.length}, ${map.length}');

    // add
    list.add('e');
    set.add('e');
    print('$list, $set');

    // remove
    list.remove('e');
    set.remove('e');
    print('$list, $set');

    // contains
    print('${list.contains('a')} ${set.contains('b')}');

    // 取值
    print(map['name']);
    // 设值
    map['height'] = 50.2;
    print(map);
    // 获取所有entries
    print('${map.entries} ${map.entries.runtimeType}');
    // 获取所有keys
    print('${map.keys} ${map.keys.runtimeType}');
    // 获取所有values
    print('${map.values} ${map.values.runtimeType}');
    // 判断包含key/value
    print('${map.containsKey('name')} ${map.containsValue(18)}');
    // 删除
    map.remove('height');
    print(map);
  }
}

// 函数
void test7() {
  print(sum(2, 3));
  print(sub(2, 3));
  print(multi(2, 3));
  print(printInfo1('张三'));

  printInfo1('张三');
  printInfo1('张三', age: 18);
  printInfo1('张三', height: 50.2);
  printInfo1('张三', age: 18, height: 50.2);
  printInfo1('张三', height: 50.2, age: 18);

  printInfo2('李四');
  printInfo2('李四', 18);
  printInfo2('李四', 18, 50.2);
  printInfo2('李四', null, 50.2);

  printInfo3('王五');

  Function sumFunc = sum;
  print(operation(5, 3, sumFunc));
  print(operation(5, 3, sub));
  print(operation(5, 3, getMultiOperation()));

  // 7.4 匿名函数
  // anonymous function, lambda, closure
  var names = ['张三', '李四', '王五', '马六'];
  // 有名函数
  forEach(element) {
    print(element);
  }
  names.forEach(forEach);
  // 匿名函数
  names.forEach((element) {
    print(element);
  });
  names.forEach((element) => print(element));

  // 7.5 词法作用域
  // 根据{}来决定作用域的范围，优先使用作用域中的变量，若没有找到则一层一层向外查找

  // 7.6 词法闭包
  // 类似于iOS中block访问外部变量
  var name = '王小虎';
  makeCallback(String callbackId) {
    return (int code, String msg) {
      print('执行回调$callbackId：<$code> $msg');
    };
  };
  var callback = makeCallback('cb_001');
  callback(0, '成功');
}

// 7.1 函数定义
int sum(int a, int b) {
  return a + b;
}
// 返回值会进行推导，可省略
// 所有函数会返回值，若没有指定返回值，则会隐式返回null
sub(int a, int b) {
  return a - b;
}
// 函数体若仅有一个表达式则可以使用箭头语法
multi(int a, int b) => a * b;

// 7.2 函数参数
// 函数参数有：必须参数、可选参数。可选参数分为：命名可选参数、位置可选参数
// 必须参数，上述普通方式定义出来的参数为必须参数
// 命名可选参数，{param1, param2, ...}，明确标示出参数的key
// 位置可选参数，[param1, param2, ...]，需要按顺序提供0个或多个参数
// 可选参数后不能定义其他参数
printInfo1(String name, {int? age, double? height}) {
  print('name=$name, age=$age, height=$height');
}
printInfo2(String name, [int? age, double? height]) {
  print('name=$name, age=$age, height=$height');
}
// 参数默认值
// 只有可选参数才能有默认值
printInfo3(String name, {int age = 12, double height = 60}) {
  print('name=$name, age=$age, height=$height');
}

// 7.3 函数是一等公民
// 意味着可以将函数本身赋值给一个变量，或作为另一个函数的参数或返回值
operation(int a, int b, Function op) {
  return op(a, b);
}
getMultiOperation() {
  return multi;
}



void main() {
  print('====================');test1();print('====================\n');
  print('====================');test2();print('====================\n');
  print('====================');test3();print('====================\n');
  print('====================');test4();print('====================\n');
  print('====================');test5();print('====================\n');
  print('====================');test6();print('====================\n');
  print('====================');test7();print('====================\n');
}